直接以dockerfile创建独立镜像

### npm

```shell
# 创建Tss-edge镜像
docker build -t tss-edge:1.0 .
# 创建Tss-edge容器,对外暴露8090端口
docker run -d --name=tss-edge -p 8090:8090  tss-edge:1.0

# 查看
docker images
docker ps -a
```
